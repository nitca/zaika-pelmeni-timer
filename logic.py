"""All program logic, buttons, timer and tray"""


from PyQt5.QtWidgets import QApplication, QWidget, QSystemTrayIcon,\
    QAction, QMenu
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QTimer
from PyQt5.QtMultimedia import QSound
from pelmeni_interface import Ui_Form


class PelmeniInterface(QWidget):
    """Interface buttons, settings and adds tray program"""
    def __init__(self):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.f_exit.hide()
        self.ui.s_exit.hide()
        self.ui.mainText.hide()

        self.tray_icon = QSystemTrayIcon()
        self.tray_icon.setIcon(QIcon('pelmeni.ico'))
        exit_action = QAction('Выход', self)

        exit_action.triggered.connect(QApplication.quit)

        tray_menu = QMenu()
        tray_menu.addAction(exit_action)

        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

        self.ui.mainBtn.clicked.connect(self.startMainTimer)
        self.ui.f_exit.clicked.connect(QApplication.quit)
        self.ui.s_exit.clicked.connect(QApplication.quit)

    def startMainTimer(self):
        """On push main button goed to tray and starts timer"""
        self.hide()
        self.tray_icon.showMessage(
            "Zaika - Варка пельменей",
            "Я засёк время, бро",
            QSystemTrayIcon.Information, 4000
            )
        self.ui.mainBtn.hide()
        self.ui.f_exit.show()
        self.ui.s_exit.show()
        self.ui.mainText.show()
        self.timer = QTimer()
        self.timer.start(420 * 1000)
        self.timer.timeout.connect(self.timeout)

    def timeout(self):
        """On stop timer shows window"""
        QSound.play('notification.wav')
        self.show()
