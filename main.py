#!/usr/bin/env python

"""Main file starts the program"""

import sys
from PyQt5.QtWidgets import QApplication
from logic import PelmeniInterface


if __name__ == '__main__':
    app = QApplication([])
    main_win = PelmeniInterface()

    main_win.show()

    sys.exit(app.exec_())
